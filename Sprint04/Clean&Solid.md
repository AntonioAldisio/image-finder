# Sprint 04 - Clean Code and & Solid

## 1. Introdução

O arquivo analisado foi o [service_tokens](debian_image_finder/web/service_tokens.py) ele é responsável por criar o token no frontend, como pode ser visto na figura abaixo.

![IMAGE-FINDER](01.png)

## 2. Clean Code

Nesse arquivo foram aplicadas dois principios de clean code. O primeiro é o do nome e o segundo a regra do escoteiro.

O primeiro consiste em adequação do nome de variáveis. No codigo foi adotado apenas uma alteração de nome.O codigo antes era apresentado assim:

```python
    if form.validate_on_submit():
        name = form.name.data
        expiration = int(form.expiration.data)
        expires_at = datetime.utcnow() + timedelta(days=expiration)
```
depois

```python
   if form.validate_on_submit():
        name = form.name.data
        days = int(form.expiration.data)
        expires_at = datetime.utcnow() + timedelta(days=days)
```
o motivo dessa alteração foi para manter o padrão adotado em todo ao longo do projeto.

O segundo principios consiste em deixar o código mais limpo de  quando você deve deixá-la mais limpa do que quando a encontrou.Sendo assim, observer quem na função generate_payload() tinha constantes que poderia ser incluidas na construição da função que era minutes e o issuer. O codigo antes era apresentado assim:


```Python
 payload = generate_payload(
            'debian-cloud-image-finder',
            minutes=1,
            extra_data={
                'user_public_id': current_user.public_id,
                'token_public_id': service_token.public_id
            }
        )

```
depois

```Python
  payload = generate_payload(
            extra_data={
                'user_public_id': current_user.public_id,
                'token_public_id': service_token.public_id
            }
        )
```
OBS: Necessário alteração em outras partes do código.


## 3. SOLID

### 3.1 Single Responsiblity Principle (Princípio da responsabilidade única)

Não foi realizado nenhuma alteração nesse topico, pois no arquivo [service_tokens](debian_image_finder/web/service_tokens.py) já obedece o princípio da responsabilidade única que é responsável apenas por açōes relacionados ao token.
