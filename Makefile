run:
	docker build -t debian-image-finder .
	docker run -it --rm \
		--env-file .env \
		-v $(PWD):/debian-image-finder \
		-p 5000:5000 \
		debian-image-finder

build:
	docker build --network host -t debian-image-finder .

shell:
	docker build . -t debian-image-finder
	docker run -it --rm \
		--env-file .env \
		--entrypoint="" \
		-v $(PWD):/debian-image-finder \
		-p 5000:5000 \
		debian-image-finder \
		bash

test-module:
	bash test-module.sh $(module-name)

test:
	docker run -it --rm \
		--env-file testing.env \
		--entrypoint="" \
		-v $(PWD):/debian-image-finder \
		debian-image-finder \
		nose2-3.8

lint:
	docker run --rm \
		--env-file .env \
		--entrypoint="" \
		-v $(PWD):/debian-image-finder \
		debian-image-finder \
		python3 -m flake8

lint-chart:
	docker run --rm \
		--env-file .env \
		--entrypoint="" \
		-v $(PWD):/debian-image-finder \
		dtzar/helm-kubectl \
		helm plugin install https://github.com/datreeio/helm-datree && \
		helm datree test charts/debian-cloud-image-finder

coverage:
	docker run --rm \
		--env-file testing.env \
		--entrypoint="" \
		-v $(PWD):/debian-image-finder \
		debian-image-finder \
		python3 -m coverage run --source='.' -m nose2

coverage-report:
	docker run --rm \
		--env-file testing.env \
		--entrypoint="" \
		-v $(PWD):/debian-image-finder \
		debian-image-finder \
		python3 -m coverage report -m

migrate-db:
	docker run -it --rm \
		--env-file .env \
		--entrypoint="" \
		-v $(PWD):/debian-image-finder \
		debian-image-finder \
		flask db migrate

upgrade-db:
	docker run -it --rm \
		--env-file .env \
		--entrypoint="" \
		-v $(PWD):/debian-image-finder \
		debian-image-finder \
		flask db upgrade

create-db:
	docker run -it --rm \
		--env-file .env \
		--entrypoint="" \
		-v $(PWD):/debian-image-finder \
		debian-image-finder \
		flask create-db

drop-db:
	docker run -it --rm \
		--env-file .env \
		--entrypoint="" \
		-v $(PWD):/debian-image-finder \
		debian-image-finder \
		flask drop-db

recreate-db:
	docker run -it --rm \
		--env-file .env \
		--entrypoint="" \
		-v $(PWD):/debian-image-finder \
		debian-image-finder \
		flask recreate-db

delete-images:
	docker run -it --rm \
		--env-file .env \
		--entrypoint="" \
		-v $(PWD):/debian-image-finder \
		debian-image-finder \
		flask delete-images

delete-users:
	docker run -it --rm \
		--env-file .env \
		--entrypoint="" \
		-v $(PWD):/debian-image-finder \
		debian-image-finder \
		flask delete-users

promote-user:
	docker run -it --rm \
		--env-file .env \
		--entrypoint="" \
		-v $(PWD):/debian-image-finder \
		debian-image-finder \
		flask promote-user

unpromote-user:
	docker run -it --rm \
		--env-file .env \
		--entrypoint="" \
		-v $(PWD):/debian-image-finder \
		debian-image-finder \
		flask unpromote-user

seed:
	docker run -it --rm \
		--env-file .env \
		--entrypoint="" \
		-v $(PWD):/debian-image-finder \
		debian-image-finder \
		flask seed run

docker-save:
	docker save debian-image-finder | gzip > debian-image-finder.tar.gz

docker-load:
	docker load < debian-image-finder.tar.gz