import sys
import unittest
import logging
from debian_image_finder.extensions.database import db
from debian_image_finder.models.user import User
from debian_image_finder.models.image import Image


def test():
    """Runs the tests without code coverage"""
    test_path = 'debian_image_finder/tests'
    tests = unittest.TestLoader().discover(test_path, pattern='test*.py')
    result = unittest.TextTestRunner(verbosity=2).run(tests)
    if result.wasSuccessful():
        return 0
    sys.exit(result)


def create_db():
    """Creates database"""
    db.create_all()


def drop_db():
    """Cleans database"""
    db.drop_all()


def recreate_db():
    """Recreates database"""
    recreate_answer = None
    while recreate_answer not in ("yes", "no"):
        recreate_answer = input("Are you sure? (yes or no) ")
    if recreate_answer == "yes":
        db.drop_all()
        db.create_all()
        db.session.commit()
        logging.info("Database recreated!")
    elif recreate_answer == "no":
        logging.info("Aborting...")
    else:
        logging.info("Please enter yes or no.")


def delete_images():
    """Delete all images from database"""
    delete_answer = None
    while delete_answer not in ("yes", "no"):
        delete_answer = input("Are you sure? (yes or no) ")
    if delete_answer == "yes":
        Image.query.delete()
        db.session.commit()
        logging.info("Images deleted!")
    elif delete_answer == "no":
        logging.info("Aborting...")
    else:
        logging.info("Please enter yes or no.")


def delete_users():
    """Delete all users from database"""
    delete_answer = None
    while delete_answer not in ("yes", "no"):
        delete_answer = input("Are you sure? (yes or no) ")
    if delete_answer == "yes":
        User.query.delete()
        db.session.commit()
        logging.info("Users deleted!")
    elif delete_answer == "no":
        logging.info("Aborting...")
    else:
        logging.info("Please enter yes or no.")


def promote_user():
    """Adds a new user to the database"""
    username = input("Username: ")

    user = User.find_by_username(username)
    user.admin = True
    db.session.add(user)
    db.session.commit()
    logging.info('User successfully updated!')


def unpromote_user():
    """Adds a new user to the database"""
    username = input("Username: ")

    user = User.find_by_username(username)
    user.admin = False
    db.session.add(user)
    db.session.commit()
    logging.info('User successfully updated!')


def init_app(app):
    # Add multiple commands in a bulk
    commands = [
        test,
        create_db,
        drop_db,
        recreate_db,
        delete_images,
        delete_users,
        promote_user,
        unpromote_user
    ]

    for command in commands:
        app.cli.add_command(app.cli.command()(command))
