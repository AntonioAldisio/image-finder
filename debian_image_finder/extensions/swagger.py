from flasgger import Swagger
from dynaconf import settings
from debian_image_finder import __version__

swagger_config = {
    'swagger': '2.0',
    'info': {
        'title': 'Cloud Image Finder API',
        'description': 'API for Debian Cloud Image Finder.',
        'contact': {
            'responsibleOrganization': 'Debian Cloud Team',
            'responsibleDeveloper': 'Arthur Diniz <arthurbdiniz@gmail.com>',
            'email': 'debian-cloud@lists.debian.org',
            'url': 'https://wiki.debian.org/Teams/Cloud',
        },
        'version': __version__
    },
    'host': settings.HOST,
    'basePath': '/api/v1',  # Base path for blueprint registration
    'schemes': settings.SCHEMES,
    'securityDefinitions': {
        'Bearer': {
            'type': 'apiKey',
            'scheme': 'bearer',
            'name': 'Authorization',
            'bearerFormat': 'JWT',
            'in': 'header',
        }
    }
}

swagger = Swagger(template=swagger_config)


def init_app(app):
    swagger.init_app(app)
