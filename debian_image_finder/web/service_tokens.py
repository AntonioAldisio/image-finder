from datetime import datetime, timedelta
from flask import session, render_template, redirect, url_for
from flask_login import login_required, current_user
from debian_image_finder.models.service_token import ServiceToken
from debian_image_finder.utils.token import generate_payload, encode_jwt
from debian_image_finder.utils.admin import admin_required
from debian_image_finder.web.forms.service_token_form import ServiceTokenForm


@login_required
@admin_required
def service_tokens():
    form = ServiceTokenForm()

    if form.validate_on_submit():
        name = form.name.data
        days = int(form.expiration.data)
        expires_at = datetime.utcnow() + timedelta(days=days)

        service_token = ServiceToken(
            name=name,
            user_id=current_user.id,
            expires_at=expires_at
        )

        service_token.save_to_db()

        payload = generate_payload(
            extra_data={
                'user_public_id': current_user.public_id,
                'token_public_id': service_token.public_id
            }
        )

        token = encode_jwt(payload)

        session['generated_token'] = token

        return redirect(url_for('.service_tokens'))

    generated_token = None

    if 'generated_token' in session:
        generated_token = session['generated_token']
        session.pop('generated_token', None)

    user_service_tokens = ServiceToken.find_all_by_user_id(current_user.id)
    return render_template('service_tokens.html',
                           form=form,
                           generated_token=generated_token,
                           user_service_tokens=user_service_tokens)
