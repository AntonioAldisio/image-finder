from flasgger import swag_from
from flask_restful import Resource
from debian_image_finder.models.image import Image
from debian_image_finder.schemas.image import images_schema


class ImagesAPI(Resource):

    @swag_from('docs/images/get.yml')
    def get(self):
        images = Image.query.all()
        return images_schema.dump(images), 200
