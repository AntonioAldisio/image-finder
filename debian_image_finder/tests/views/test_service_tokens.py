import unittest
from flask import url_for
from debian_image_finder.tests.views.base import TestBase


class TestServiceTokensView(TestBase):

    def test_service_tokens_view(self):
        """
        Test that service tokens page is inaccessible without login
        and redirects to index page.
        """
        response = self.client.get(
            url_for('.service_tokens')
        )
        redirect_url = url_for('.index')
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, redirect_url)

    def test_service_tokens_delete_view(self):
        """
        Test that service token delete page is inaccessible without login
        and redirects to index page.
        """
        response = self.client.get(
            url_for('.delete_service_token', token_public_id=self.service_token.public_id)
        )
        redirect_url = url_for('.index')
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, redirect_url)


if __name__ == '__main__':
    unittest.main()
