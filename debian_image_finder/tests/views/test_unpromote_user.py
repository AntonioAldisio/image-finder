import unittest
from flask import url_for
from debian_image_finder.tests.views.base import TestBase


class TestUnpromoteUserView(TestBase):

    def test_unpromote_user_view(self):
        """
        Test that unpromote user page is inaccessible without login
        and redirects to index page.
        """
        response = self.client.get(
            url_for('.unpromote_user', public_id=self.user.public_id)
        )
        redirect_url = url_for('.index')
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, redirect_url)


if __name__ == '__main__':
    unittest.main()
