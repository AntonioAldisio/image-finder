import json
import unittest
from debian_image_finder.tests.api.base import TestBase


class TestUserApi(TestBase):
    """Tests for the User Service."""

    def test_get_user(self):
        """Ensure the /user route behaves correctly."""

        headers = {
            'Content-Type': 'application/json',
            'Authorization': f'Bearer {self.token}'
        }

        response = self.client.get(
            f'/api/v1/user/{self.user.public_id}',
            headers=headers
        )

        data = json.loads(response.data.decode())
        self.assertEqual(response.status_code, 200)
        self.assertIn('user', data['name'])
        self.assertEqual(False, data['admin'])
        self.assertEqual(self.user.public_id, data['public_id'])

    def test_promote_user(self):
        """Ensure the promote user route behaves correctly."""

        headers = {
            'Content-Type': 'application/json',
            'Authorization': f'Bearer {self.token}'
        }

        response = self.client.put(
            f'/api/v1/user/promote/{self.user.public_id}',
            headers=headers
        )

        data = json.loads(response.data.decode())
        self.assertEqual(response.status_code, 200)
        self.assertIn('The user has been promoted.', data['message'])

    def test_unpromote_user(self):
        """Ensure the unpromote user route behaves correctly."""

        headers = {
            'Content-Type': 'application/json',
            'Authorization': f'Bearer {self.token}'
        }

        response = self.client.put(
            f'/api/v1/user/unpromote/{self.user.public_id}',
            headers=headers
        )

        data = json.loads(response.data.decode())
        self.assertEqual(response.status_code, 200)
        self.assertIn('The user has been unpromoted.', data['message'])

    def test_delete_user(self):
        """Ensure the delete user route behaves correctly."""

        headers = {
            'Content-Type': 'application/json',
            'Authorization': f'Bearer {self.token}'
        }

        response = self.client.delete(
            f'/api/v1/user/{self.user.public_id}',
            headers=headers
        )

        data = json.loads(response.data.decode())
        self.assertEqual(response.status_code, 200)
        self.assertIn('The user has been deleted.', data['message'])


if __name__ == '__main__':
    unittest.main()
