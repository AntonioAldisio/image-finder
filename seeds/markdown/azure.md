#### Support
Support for Azure should be requested via the Azure Support Portal. [How to create an Azure support request?](https://docs.microsoft.com/en-us/azure/azure-supportability/how-to-create-azure-support-request)

If there are general questions about the image itself, those can be asked via [mailto:debian-cloud@lists.debian.org](debian-cloud@lists.debian.org)

#### Images
Debian images are available in all Public Azure regions, as well as Azure China, Azure Germany and Azure USGov clouds.

There are several different images available:

- [Debian 8 ("Jessie")](https://azuremarketplace.microsoft.com/en-us/marketplace/apps/credativ.debian?tab=PlansAndPrice)

- [Debian 8 ("Jessie") with backports kernel](https://azuremarketplace.microsoft.com/en-us/marketplace/apps/credativ.debian?tab=PlansAndPrice)

- [Debian 9 ("Stretch")](https://azuremarketplace.microsoft.com/en-us/marketplace/apps/credativ.debian?tab=PlansAndPrice)

- [Debian 9 ("Stretch") with backports kernel](https://azuremarketplace.microsoft.com/en-us/marketplace/apps/credativ.debian?tab=PlansAndPrice)

- [Debian 10 ("Buster")](https://azuremarketplace.microsoft.com/en-us/marketplace/apps/debian.debian-10?tab=PlansAndPrice)

#### Access via the Azure portal
Debian can be found in the Marketplace via the new resource mechanism of the Azure portal.

#### Access via Azure Resource Manager (ARM)

The Debian images can be found using the following parts of the image URN:

Publisher: `credativ`

Offer: `Debian`

SKU: `8, 8-backports, 9, 9-backports`

Version: `latest or a valid version`

With Debian 10 ("Buster") Debian images have been moved to the publisher "Debian":

Publisher: `Debian`

Offer: `debian-10`

SKU: `10`

Version: `latest or a valid version`

An example for the final resource name is `credativ:Debian:9:latest`.

#### Access via Azure stack

Debian is available for syndication from the public Azure Marketplace and can be imported this way directly into the local Azure Stack installation.

#### Image build automation

Debian VMs in Azure require the device drivers for Hyper-V (hv_*.ko), which are already available with Debian's Jessie kernel. In addition, Azure images require a provisioning daemon to be preinstalled on the image, called the [Azure Linux agent](https://packages.debian.org/sid/waagent). See also [Azure's documentation](https://docs.microsoft.com/en-us/azure/virtual-machines/linux/debian-create-upload-vhd) for building Debian images.

Scripts to build Debian's Azure images: [https://github.com/credativ/azure-manage](https://github.com/credativ/azure-manage).

Jenkins infrastructure for building Azure images: [azure-build.debian.net](https://azure-build.debian.net/)

#### Mirrors
There exists high available and distributed Debian mirrors inside Azure. They provide anything for architectures `amd64`, `i386` and `source` from main and security archive and a temporary local repository.

```
deb http://debian-archive.trafficmanager.net/debian jessie main
deb http://debian-archive.trafficmanager.net/debian-security jessie/updates main
deb-src http://debian-archive.trafficmanager.net/debian jessie main
deb-src http://debian-archive.trafficmanager.net/debian-security jessie/updates main
```

The temporary debian-azure repository is signed using the following key. This key can be found also in the `debian-azure-archive-keyring` package.

### <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
```
debian-azure archive for Stretch

For Debian Stretch, debian-azure archive on debian-archive.trafficmanager.net is not needed any more, all needed packages are now in Debian's main archive.
```

---

```
-----BEGIN PGP PUBLIC KEY BLOCK-----
Version: GnuPG v2

mQENBFYWcbkBCADKvo4KT/ic3wb4AoNFtWvoS1Ae3NFcvM/6WR/n9BTsayOlnjDu
EFh7yH16nIhuO5pZFocY+32BWMEmiK6/0pkk1lB9+XfNB8yqDCJ/ItzADfuKQv6g
smfx4tLa1Vj+jUbrgiIG7MYTCPlpkSuPA+FgntlR4JBxOy2g4fqqp3of+xM8tfSN
4I7/g1989YU9EQN53c1HGLdsc5x5Y5Kezd46H4IK5oyri7BaG4zRzpJLYa4yxB2W
re+HXP0a5SN/zQq1oq8pvkPubEBPKhdypZzzoHWPkZqXh1pHsMMNRkwsLW/3iwzE
IjDKrbdILz3/b+iLLIvYo9RlTycRO0XeS/B3ABEBAAG0UkRlYmlhbiBmb3IgQXp1
cmUgQXJjaGl2ZSBBdXRvbWF0aWMgU2lnbmluZyBLZXkgKDgvamVzc2llKSA8bWlj
cm9zb2Z0QGNyZWRhdGl2LmNvbT6JAT4EEwECACgFAlYWcbkCGwMFCQPCZwAGCwkI
BwMCBhUIAgkKCwQWAgMBAh4BAheAAAoJEAbqSemobK1/2c8H/1j4HK/izt7aw7Kk
g+ChRDhltb4LiYTZNgHaBnLGkKewkA1vYIHQjdu/pVnJjh1JzvOBFV+rhJ4Cc60D
s4JDqkrl8LpA4jsLb/PW1bRtbW92mPfhWgjyWs6S2tRBsBy1m2+SLZKeaDUq/PCD
VnaClzDh2fQYimI8IQk5U/u8VHXtDm3qYQSq7dJMTTnDcBC1oCjEYN+uPm9M+tUI
ik2hIE4VmjIXYTQVTa1lD+qyJvZ9hCsBUxAGllFpcWlyexN5orer6c+30XY/0Y4i
Q+zzVPrp1s6MgM9F8ofgd2+MhL57fwyj2A+Tbyzm7xYhzPPMx/mopo4pi884+5lo
FY6A20E=
=X0jG
-----END PGP PUBLIC KEY BLOCK-----
```

#### Known problems
**Expired Debian for Azure Archive Automatic Signing Key (Jessie)**

On Debian Jessie images, that were created before February 2nd 2017, we included an extra repository called "debian-azure" (URL: [http://debian-archive.trafficmanager.net/debian-azure](http://debian-archive.trafficmanager.net/debian-azure)) to ship an updated version of waagent. The archive signing key for that repository expired.

This repository is not needed any more and can savely be removed from the sources.list. The updated waagent package is nowadays shipped with the debian-backports repository.